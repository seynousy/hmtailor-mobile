import React, { Component } from "react";
import { View, AsyncStorage } from "react-native";
import { NavigationActions } from "react-navigation";
import colors from "~theme/colors";
import clientStore from "../../stores/clientStore";
import { observer } from "mobx-react";

@observer
export default class LoginGuard extends Component {
  static navigationOptions = {
    header: null
  };
  async componentDidMount() {
    try {
      let targetScreen = "Dashboard";
      let token = await AsyncStorage.getItem("TOKEN_ID");
      console.log(token);
      if (token === null) {
        targetScreen = "Onboarding";
      } else {
        clientStore.getTailorbyId(token);
      }
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate(
            { routeName: targetScreen },
            { tailor: clientStore.currentTailorDetails }
          )
        ]
      });
      this.props.navigation.dispatch(resetAction);
    } catch (e) {
      alert("Une erreur inattendue est survenue");
      console.error(e);
    }
  }
  render() {
    return <View style={{ backgroundColor: colors.WHITE, flex: 1 }} />;
  }
}
