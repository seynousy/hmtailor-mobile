import React, { Component } from "react";
import {
  View,
  Text,
  Platform,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/common/Button";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import colors from "~theme/colors";
import { NavigationActions } from "react-navigation";
import clientStore from "../../stores/clientStore";
import { observer } from "mobx-react";
import * as mobx from "mobx";

@observer
export default class ClientDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: this.props.navigation.state.params.client.prenom,
      clientMensurations: []
    };
  }

  static navigationOptions({ navigation }) {
    return {
      title: navigation.state.params.client.prenom,
      headerRight: (
        <TouchableOpacity
          style={{ padding: 10 }}
          onPress={() => {
            const { client } = navigation.state.params;
            const goBack = NavigationActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
            });
            Alert.alert(
              "Voulez vous vraiment suprrimer cet utilisateur",
              client.prenom + " " + client.nom,
              [
                {
                  text: "Annuler",
                  onPress: () => {},
                  style: "cancel"
                },
                {
                  text: "Oui",
                  onPress: () => {
                    clientStore.removeTailorClient(client.id);
                    navigation.dispatch(goBack);
                  }
                }
              ],
              { cancelable: true }
            );
          }}
        >
          <View>
            <Icon
              name={Platform.OS === "ios" ? "ios-trash" : "md-trash"}
              size={30}
              color={colors.WHITE}
            />
          </View>
        </TouchableOpacity>
      )
    };
  }

  async componentDidMount() {
    await clientStore.getClientMensurations(
      this.props.navigation.state.params.client.id
    );
    let mensurations = mobx.toJS(clientStore.clientMensurations);
    this.setState({ clientMensurations: mensurations });
  }

  // handleDelete = () => {
  //   const { client } = this.props.navigation.state.params;
  //   const goBack = NavigationActions.reset({
  //     index: 0,
  //     actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
  //   });
  //   Alert.alert(
  //     "Voulez vous vraiment suprrimer cet utilisateur",
  //     client.prenom + " " + client.nom,
  //     [
  //       {
  //         text: "Annuler",
  //         onPress: () => {},
  //         style: "cancel"
  //       },
  //       {
  //         text: "Oui",
  //         onPress: () => {
  //           clientStore.removeTailorClient(client.id);
  //           this.props.navigation.dispatch(goBack);
  //         }
  //       }
  //     ],
  //     { cancelable: true }
  //   );
  // };

  render() {
    let { client } = this.props.navigation.state.params;
    return (
      <ParallaxScrollView
        backgroundColor={colors.PRIMARY}
        contentBackgroundColor={colors.WHITE}
        parallaxHeaderHeight={250}
        renderForeground={() => (
          <View
            style={{
              height: 250,
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Icon
              name={Platform.OS === "ios" ? "ios-person" : "md-person"}
              size={200}
              color={colors.WHITE}
            />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "90%"
              }}
            >
              <Text style={{ color: colors.WHITE, fontSize: 20 }}>
                {client.prenom + " " + client.nom}
              </Text>
              <TouchableOpacity onPress={() => {}}>
                <View>
                  <Icon
                    name={Platform.OS === "ios" ? "ios-create" : "md-create"}
                    size={30}
                    color={colors.WHITE}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
      >
        <View style={{ flex: 1, padding: 10, minHeight: 350 }}>
          <Text
            style={{
              fontSize: 20,
              color: colors.PRIMARY,
              textAlign: "center"
            }}
          >
            Informations Personnelles
          </Text>
          <Text>Prénom: {client.prenom}</Text>
          <Text>Nom: {client.nom}</Text>
          <Text>Téléphone: {client.numtel}</Text>
          <Text>Adresse: {client.adresse}</Text>
          <Text>Email: {client.email}</Text>
          <Text>Sexe: {client.sexe === "M" ? "Masculin" : "Féminin"}</Text>
          <Text
            style={{
              fontSize: 20,
              color: colors.PRIMARY,
              textAlign: "center"
            }}
          >
            Mensurations
          </Text>
          {this.state.clientMensurations.map((c, i) => (
            <Text key={i}>{c.mensuration.nom + ": " + c.valeur}</Text>
          ))}
        </View>
        {/* <View style={{ justifyContent: "flex-end" }}>
          <Button
            title="Supprimer"
            titleColor={Platform.OS === "ios" ? "red" : colors.WHITE}
            color="red"
            onPress={() => this.handleDelete()}
          />
        </View> */}
      </ParallaxScrollView>
    );
  }
}
