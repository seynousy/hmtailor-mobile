import React, { Component } from "react";
import {
  View,
  Platform,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  StyleSheet
} from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Ionicons";
import colors from "~theme/colors";

export default class Client extends Component {
  _renderChildren() {
    const { client } = this.props;
    return (
      <View style={styles.container}>
        <Icon
          name={Platform.OS === "ios" ? "ios-contact" : "md-contact"}
          size={60}
          color={colors.SECONDARY}
        />
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={styles.text}>
            {client.prenom + " " + client.nom}
          </Text>
        </View>
        {Platform.OS === "ios" ? (
          <Icon
            name={"ios-arrow-forward"}
            size={60}
            color={colors.SECONDARY_LIGHT}
          />
        ) : null}
      </View>
    );
  }
  render() {
    const { client } = this.props;
    return Platform.OS === "ios" ? (
      <TouchableOpacity activeOpacity={0.5} onPress={this.props.onPress}>
        {this._renderChildren()}
      </TouchableOpacity>
    ) : (
      <TouchableNativeFeedback onPress={this.props.onPress}>
        {this._renderChildren()}
      </TouchableNativeFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textContainer: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    marginLeft: 10
  },
  text: {
    fontSize: 17
  }
});

Client.PropTypes = {
  client: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  })
};
