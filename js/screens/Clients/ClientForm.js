import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { ScrollView, Text, View, Platform } from "react-native";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import Input from "../../components/common/Input";
import Button from "../../components/common/Button";
import colors from "~theme/colors";

import clientStore from "../../stores/clientStore";
import { observer } from "mobx-react";

const radio_props = [
  { label: "  Homme  ", value: "M" },
  { label: "  Femme  ", value: "F" }
];

@observer
export default class ClientForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      address: "",
      gender: "",
      phone: "",
      email: ""
    };
  }

  static navigationOptions = {
    title: "Nouveau Client"
  };

  _submitForm() {
    const goBack = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
    });
    apiClient = {
      idTailleur: clientStore.currentTailor,
      prenom: this.state.firstname,
      nom: this.state.lastname,
      sexe: this.state.gender,
      email: this.state.email,
      numtel: this.state.phone,
      adresse: this.state.address
    };
    console.log(apiClient);
    clientStore.addTailorClient(apiClient);
    this.props.navigation.dispatch(goBack);
  }

  render() {
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: colors.WHITE, padding: 10 }}
        contentContainerStyle={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Input
          placeholder="Prénom"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-person" : "md-person"}
          onChangeText={text => this.setState({ firstname: text })}
          value={this.state.firstname}
        />
        <Input
          placeholder="Nom"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-person" : "md-person"}
          onChangeText={text => this.setState({ lastname: text })}
          value={this.state.lastname}
        />
        <Input
          placeholder="Adresse"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-pin" : "md-pin"}
          onChangeText={text => this.setState({ address: text })}
          value={this.state.address}
        />
        <View>
          <RadioForm
            style={{ padding: 10, alignSelf: "center" }}
            formHorizontal={true}
            radio_props={radio_props}
            initial={-1}
            buttonColor={colors.PRIMARY_DARK}
            labelColor={colors.BLACK}
            selectedButtonColor={colors.PRIMARY_DARK}
            selectedLabelColor={colors.BLACK}
            onPress={value => {
              this.setState({ gender: value });
            }}
          />
        </View>
        <Input
          placeholder="Téléphone"
          keyboardType="phone-pad"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-call" : "md-call"}
          onChangeText={text => this.setState({ phone: text })}
          value={this.state.phone}
        />
        <Input
          placeholder="Email"
          keyboardType="email-address"
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          icon={Platform.OS === "ios" ? "ios-mail" : "md-mail"}
          onChangeText={text => this.setState({ email: text })}
          value={this.state.email}
        />
        <Button
          title="Enregistrer"
          color={colors.PRIMARY}
          titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
          style={{ width: 150, borderRadius: 2, marginTop: 15, elevation: 2 }}
          onPress={() => this._submitForm()}
        />
      </ScrollView>
    );
  }
}
