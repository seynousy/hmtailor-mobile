import React, { Component } from "react";
import { View, Platform, AsyncStorage } from "react-native";
import ClientsList from "./ClientsList";
import Icon from "react-native-vector-icons/Ionicons";
import ActionButton from "react-native-action-button";
import { SearchBar } from "react-native-elements";
import clientStore from "../../stores/clientStore";
import colors from "~theme/colors";
import { observer } from "mobx-react";

@observer
export default class Clients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clients: []
    };
  }
  static navigationOptions = {
    title: "Clients",
    tabBarLabel: "Clients",
    headerTitleStyle: { alignSelf: "center" },
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name={Platform.OS === "ios" ? "ios-people" : "md-people"}
        size={30}
        color={tintColor}
      />
    )
  };
  async componentDidMount() {
    let token = await AsyncStorage.getItem("TOKEN_ID");
    await clientStore.getTailorClients(token);
    this.setState({ clients: clientStore.clients });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <SearchBar
          lightTheme
          onChangeText={() => {}}
          onClearText={() => {}}
          placeholder="Rechercher ..."
          inputStyle={{
            backgroundColor: colors.WHITE
          }}
          containerStyle={{ backgroundColor: colors.WHITE }}
          clearIcon
        />
        <View
          style={{
            flex: 1,
            backgroundColor: colors.WHITE,
            paddingLeft: 15,
            paddingRight: 15
          }}
        >
          <ClientsList
            data={clientStore.clients}
            onPress={client => {
              this.props.navigation.navigate("ClientDetails", { client });
            }}
          />
          <ActionButton
            buttonColor={colors.SECONDARY_LIGHT}
            onPress={() => this.props.navigation.navigate("ClientForm")}
            icon={
              <Icon
                name={
                  Platform.OS === "ios" ? "ios-person-add" : "md-person-add"
                }
                color={colors.WHITE}
                size={24}
              />
            }
            fixNativeFeedbackRadius={true}
          />
        </View>
      </View>
    );
  }
}
