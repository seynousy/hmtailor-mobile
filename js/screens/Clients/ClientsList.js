import React, { Component } from "react";
import { FlatList, View } from "react-native";
import Client from "./Client";
import colors from "~theme/colors";

export default class ClientList extends Component {
  _renderItem = ({ item }) => (
    <Client onPress={() => this.props.onPress(item)} client={item} />
  );

  _keyExtractor = (item, index) => item.id;

  _renderSeparator = () => (
    <View style={{ height: 0.5, borderWidth: 0.5, marginLeft: 50 }} />
  );

  render() {
    return (
      <FlatList
        data={this.props.data}
        extraData={this.state}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        ItemSeparatorComponent={this._renderSeparator}
      />
    );
  }
}
