import React, { Component } from "react";
import { View, Text, Platform, AsyncStorage } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { NavigationActions } from "react-navigation";
import SettingsList from "react-native-settings-list";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class Settings extends Component {
  static navigationOptions = {
    title: "Paramètres"
  };

  async logout() {
    await AsyncStorage.removeItem("TOKEN_ID");
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Authentification" })]
    });
    this.props.navigation.dispatch(resetAction);
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: colors.WHITE,
          paddingLeft: 15,
          paddingRight: 15
        }}
      >
        <SettingsList borderColor={colors.GREY}>
          <SettingsList.Item
            hasNavArrow={false}
            title="Compte"
            titleStyle={{
              color: colors.SECONDARY,
              marginTop: 30,
              fontWeight: "500",
              fontSize: 17
            }}
            itemWidth={50}
            borderHide={"Both"}
          />
          <SettingsList.Item
            icon={
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Icon
                  size={30}
                  name={Platform.OS === "ios" ? "ios-person" : "md-person"}
                  color={colors.PRIMARY}
                />
              </View>
            }
            hasNavArrow={true}
            title={
              clientStore.currentTailorDetails.prenom +
              " " +
              clientStore.currentTailorDetails.nom
            }
            titleStyle={{
              color: colors.BLACK,
              fontSize: 17
            }}
            onPress={() => {}}
            underlayColor="gray"
          />
          <SettingsList.Item
            icon={
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Icon
                  size={30}
                  name={Platform.OS === "ios" ? "ios-log-out" : "md-log-out"}
                  color={colors.PRIMARY}
                />
              </View>
            }
            hasNavArrow={true}
            title="Se déconnecter"
            titleStyle={{
              color: colors.BLACK,
              fontSize: 17
            }}
            onPress={() => this.logout()}
            underlayColor="gray"
          />
          <SettingsList.Item
            hasNavArrow={false}
            title="Mensurations"
            titleStyle={{
              color: colors.SECONDARY,
              marginTop: 30,
              fontWeight: "500",
              fontSize: 17
            }}
            itemWidth={50}
            borderHide={"Both"}
          />
          <SettingsList.Item
            icon={
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Icon
                  size={30}
                  name={Platform.OS === "ios" ? "ios-add" : "md-add"}
                  color={colors.PRIMARY}
                />
              </View>
            }
            hasNavArrow={true}
            title="Ajouter des mensurations"
            titleStyle={{
              color: colors.BLACK,
              fontSize: 17
            }}
            onPress={() => this.props.navigation.navigate("MensurationForm")}
            underlayColor="gray"
          />
          <SettingsList.Item
            icon={
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Icon
                  size={30}
                  name={Platform.OS === "ios" ? "ios-list-box" : "md-list-box"}
                  color={colors.PRIMARY}
                />
              </View>
            }
            hasNavArrow={true}
            title="Liste des mensurations"
            titleStyle={{
              color: colors.BLACK,
              fontSize: 17
            }}
            onPress={() => this.props.navigation.navigate("Mensurations")}
            underlayColor="gray"
          />
        </SettingsList>
      </View>
    );
  }
}
