import React, { Component } from "react";
import {
  View,
  Platform,
  ScrollView,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import CatalogFolder from "../../components/common/CatalogFolder";
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import * as mobx from "mobx";
import clientStore from "../../stores/clientStore";

// let id = 0;
// const catalogs = [
//   {
//     id: id++,
//     label: "Fashion"
//   },
//   {
//     id: id++,
//     label: "Traditionnel"
//   },
//   {
//     id: id++,
//     label: "Class"
//   },
//   {
//     id: id++,
//     label: "Diplomate"
//   }
// ];

@observer
export default class Catalog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      catalogs: [],
      loading: false
    };
  }

  async componentWillMount() {
    this.setState({ loading: true });
    let token = await AsyncStorage.getItem("TOKEN_ID");
    await clientStore.getTailorCatalogs(token);
    this.setState({ catalogs: clientStore.tailorCatalogs.slice() });
    this.setState({ loading: false });
  }
  static navigationOptions = {
    title: "Catalogue",
    tabBarLabel: "Dashboard",
    headerTitleStyle: { alignSelf: "center" },
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name={Platform.OS === "ios" ? "ios-book" : "md-book"}
        size={30}
        color={tintColor}
      />
    )
  };
  _handlePress = catalog => {
    let catalogPassed = mobx.toJS(catalog);
    console.log(catalogPassed);
    this.props.navigation.navigate("CatalogDetails", { catalogPassed });
  };
  render() {
    return (
      <ScrollView
        style={{ backgroundColor: colors.WHITE, flex: 1 }}
        contentContainerStyle={{
          flex: 1,
          backgroundColor: colors.WHITE,
          flexWrap: "wrap",
          flexDirection: "row",
          alignItems: "flex-start"
        }}
      >
        {this.state.loading === true ? (
          <ActivityIndicator size="large" color={colors.SECONDARY} />
        ) : (
          this.state.catalogs.map(catalog => (
            <CatalogFolder
              onPress={() => this._handlePress(catalog)}
              label={catalog.nom}
              key={catalog.id}
            />
          ))
        )}
        <ActionButton
          buttonColor={colors.SECONDARY_LIGHT}
          onPress={() => this.props.navigation.navigate("CatalogForm")}
          fixNativeFeedbackRadius={true}
        />
      </ScrollView>
    );
  }
}
