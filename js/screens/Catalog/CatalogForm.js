import React, { Component } from "react";
import {
  ScrollView,
  View,
  StyleSheet,
  Platform,
  AsyncStorage
} from "react-native";
import Input from "../../components/common/Input";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import { NavigationActions } from "react-navigation";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class CatalogForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: ""
    };
  }

  async _handleSubmit() {
    const gotoCatalogs = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
    });
    let id = await AsyncStorage.getItem("TOKEN_ID");
    console.log("ID" + id);
    let data = {
      nom: this.state.nom,
      idTailleur: id
    };
    await clientStore.addCatalog(data);
    this.props.navigation.dispatch(gotoCatalogs);
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Input
          placeholder="Nom Catalogue"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-shirt" : "md-shirt"}
          onChangeText={text => this.setState({ nom: text })}
          value={this.state.nom}
        />
        <Button
          title="Enregistrer"
          color={colors.PRIMARY}
          titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
          style={{
            width: 150,
            borderRadius: 2,
            marginTop: 15,
            elevation: 2,
            alignSelf: "center"
          }}
          onPress={() => this._handleSubmit()}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    backgroundColor: colors.WHITE
    // justifyContent: "center",
    // alignItems: "center"
  }
});
