import React, { Component } from "react";
import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity
} from "react-native";
import ActionButton from "react-native-action-button";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import * as mobx from "mobx";
import clientStore from "../../stores/clientStore";
let LOCAL_URL =
  Platform.OS === "ios"
    ? "http://localhost:8080/"
    : "http://192.168.43.107:8080/";
// const images = [
//   "https://placehold.it/100x100",
//   "https://placehold.it/100x100",
//   "https://placehold.it/100x100",
//   "https://placehold.it/100x100"
// ];

@observer
export default class CatalogDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      models: []
    };
  }
  static navigationOptions({ navigation }) {
    return {
      title: navigation.state.params.catalogPassed.nom
    };
  }
  async componentDidMount() {
    await clientStore.getCatalogModels(
      this.props.navigation.state.params.catalogPassed.id
    );
    let models = mobx.toJS(clientStore.catalogModels);
    this.setState({ models: models });
  }
  render() {
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: colors.WHITE }}
        contentContainerStyle={styles.container}
      >
        {this.state.models.map((model, index) => (
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("ModelDetails", { model: model })
            }
            style={{
              borderWidth: 1,
              height: 100,
              width: 100,
              borderColor: colors.SECONDARY
            }}
            key={index}
          >
            <View>
              <Image
                resizeMode="contain"
                style={styles.image}
                source={{ uri: LOCAL_URL + model.pathToImage }}
              />
              <Text style={{ textAlign: "center" }}>{model.nom}</Text>
            </View>
          </TouchableOpacity>
        ))}
        <ActionButton
          buttonColor={colors.SECONDARY_LIGHT}
          onPress={() =>
            this.props.navigation.navigate("ModelForm", {
              catalogId: this.props.navigation.state.params.catalogPassed.id
            })
          }
          fixNativeFeedbackRadius={true}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 2,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around"
  },
  image: {
    width: 75,
    height: 75,
    margin: 1
  }
});
