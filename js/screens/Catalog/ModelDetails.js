import React, { Component } from "react";
import {
  View,
  Text,
  TouchableHighlight,
  Platform,
  TextInput,
  Alert,
  Image
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/common/Button";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import colors from "~theme/colors";
import { NavigationActions } from "react-navigation";
import clientStore from "../../stores/clientStore";
import { observer } from "mobx-react";

let URL =
  Platform.OS === "ios"
    ? "http://localhost:8080/"
    : "http://192.168.43.107:8080/";

@observer
export default class ModelDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  static navigationOptions({ navigation }) {
    return {
      title: navigation.state.params.model.nom
    };
  }

  // handleDelete = () => {
  //   const { model } = this.props.navigation.state.params;
  //   const goBack = NavigationActions.reset({
  //     index: 0,
  //     actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
  //   });
  // };

  render() {
    let { model } = this.props.navigation.state.params;
    return (
      <View style={{ backgroundColor: colors.WHITE, flex: 1 }}>
        <ParallaxScrollView
          backgroundColor={colors.WHITE}
          contentBackgroundColor={colors.WHITE}
          parallaxHeaderHeight={250}
          renderForeground={() => (
            <View
              style={{
                height: 250,
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{ width: 200, height: 200 }}>
                <Image
                  style={{ width: 200, height: 200 }}
                  source={{ uri: URL + model.pathToImage }}
                />
              </View>
              <Text style={{ color: colors.WHITE, fontSize: 20 }}>
                {model.nom}
              </Text>
            </View>
          )}
        >
          <Text>Nom: {model.nom}</Text>
          <View
            style={{
              padding: 10,
              height: 300,
              justifyContent: "flex-end"
            }}
          >
            <View style={{}}>
              <Button
                title="Commander"
                titleColor={Platform.OS === "ios" ? "red" : colors.WHITE}
                color={colors.SECONDARY}
                onPress={() =>
                  this.props.navigation.navigate("OrderForm", {
                    model: model
                  })
                }
              />
            </View>
          </View>
        </ParallaxScrollView>
      </View>
    );
  }
}
