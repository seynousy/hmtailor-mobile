import React, { Component } from "react";
import {
  ScrollView,
  View,
  StyleSheet,
  Platform,
  Text,
  Image
} from "react-native";
import ModalSelector from "react-native-modal-selector";
import { NavigationActions } from "react-navigation";
import Input from "../../components/common/Input";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import ImagePicker from "react-native-image-crop-picker";
import * as mobx from "mobx";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class ModelForm extends Component {
  static navigationOptions = {
    title: "Ajout de modèle"
  };
  constructor(props) {
    super(props);
    this.state = {
      nom: "",
      description: "",
      prix: "",
      selectedType: "",
      idTypeDhabit: null,
      dressTypes: [],
      imagePath: ""
    };
  }

  async componentDidMount() {
    await clientStore.getDressTypes();
    this.setState({ dressTypes: mobx.toJS(clientStore.dressTypes) });
  }

  _handleImagePick() {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      this.setState({ imagePath: image.path, image: image });
      console.log(image);
    });
  }

  async _handleSubmit() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
    });
    let data = {
      nom: this.state.nom,
      description: this.state.description,
      prix: this.state.prix,
      idTypeDhabit: this.state.idTypeDhabit,
      idCatalogue: this.props.navigation.state.params.catalogId,
      image: this.state.image
    };
    await clientStore.addModel(data);
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    let index = 0;
    const data = [{ key: index++, section: true, label: "Types d'habits" }];

    for (let i = 0; i < this.state.dressTypes.length; i++) {
      let entry = {
        key: index++,
        label: this.state.dressTypes[i].nom,
        id: this.state.dressTypes[i].id
      };
      data.push(entry);
    }
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Input
          placeholder="Nom du modèle"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-shirt" : "md-shirt"}
          onChangeText={text => this.setState({ nom: text })}
          value={this.state.nom}
        />
        <Input
          placeholder="Description"
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-create" : "md-create"}
          onChangeText={text => this.setState({ description: text })}
          value={this.state.description}
          multiline={true}
          numberOfLines={8}
          style={{ height: 100, backgroundColor: colors.PRIMARY_DARK }}
        />
        <Input
          placeholder="Prix"
          keyboardType="numeric"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-pricetag" : "md-pricetag"}
          onChangeText={text => this.setState({ prix: text })}
          value={this.state.prix}
        />
        <ModalSelector
          data={data}
          initValue=""
          onChange={option => {
            this.setState({
              idTypeDhabit: option.id,
              selectedType: option.label
            });
          }}
        >
          <Input
            placeholder="Type d'habit"
            style={{ backgroundColor: colors.PRIMARY_DARK }}
            iconStyle={{ backgroundColor: colors.PRIMARY }}
            icon={Platform.OS === "ios" ? "ios-shirt" : "md-shirt"}
            value={this.state.selectedType}
          />
        </ModalSelector>

        <Button
          title="Choisir une photo"
          color={colors.PRIMARY}
          titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
          style={{
            width: 150,
            borderRadius: 2,
            marginTop: 15,
            elevation: 2,
            alignSelf: "center"
          }}
          onPress={() => this._handleImagePick()}
        />
        <View style={{ width: 100, height: 100 }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={{
              uri:
                this.state.imagePath.length === 0
                  ? "http://placehold.it/100x100"
                  : this.state.imagePath
            }}
          />
        </View>
        <Button
          title="Enregistrer"
          color={colors.PRIMARY}
          titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
          style={{
            width: 150,
            borderRadius: 2,
            marginTop: 15,
            elevation: 2,
            alignSelf: "center"
          }}
          onPress={() => this._handleSubmit()}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    backgroundColor: colors.WHITE
    // justifyContent: "center",
    // alignItems: "center"
  }
});
