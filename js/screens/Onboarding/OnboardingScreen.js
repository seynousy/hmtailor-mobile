import React, { Component } from "react";
import { Image, View, StyleSheet, Platform, Text } from "react-native";
import PropTypes from "prop-types";
import colors from "../../theme/colors";

export default class OnboardingScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            backgroundColor: colors.PRIMARY
          }}
        >
          <Image source={this.props.image} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.desc}>{this.props.desc}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.PRIMARY,
    alignItems: "center",
    flex: 1
  },
  textContainer: {
    backgroundColor: colors.PRIMARY,
    alignItems: "center",
    flex: 1,
    width: "80%",
    marginTop: "10%"
  },
  title: {
    textAlign: "center",
    padding: 20,
    fontSize: 20,
    fontWeight: "bold",
    color: colors.WHITE
  },
  desc: {
    textAlign: "center",
    color: colors.WHITE
  }
});

OnboardingScreen.propTypes = {
  image: PropTypes.oneOfType([
    PropTypes.shape({
      uri: PropTypes.string
    }),
    PropTypes.number
  ]),
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired
};
