export default (steps = [
  {
    id: 0,
    image: require("../../../assets/images/logo.png"),
    title: "Laissez-nous vous assister !",
    desc: "Concentrez-vous sur votre travail"
  },
  {
    id: 1,
    image: require("../../../assets/images/mensurations.png"),
    title: "Enregistrez vos clients et leurs mensurations",
    desc: "Ne perdez plus les mensurations de vos clients"
  },
  {
    id: 2,
    image: require("../../../assets/images/models.png"),
    title: "Proposez vos catalogues de modèles à confectionner",
    desc: "Ajoutez une multitude de modèles à vos catalogues personnalisés"
  }
]);
