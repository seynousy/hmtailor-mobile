import { StyleSheet, View, Text, Platform } from "react-native";
import React, { Component } from "react";
import { IndicatorViewPager, PagerDotIndicator } from "rn-viewpager";
import { NavigationActions } from "react-navigation";
import OnboardingScreen from "./OnboardingScreen";
import Button from "../../components/common/Button";
import colors from "../../theme/colors";
import steps from "./onboardingStepsConfig";

export default class Onboarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 0
    };
  }

  static navigationOptions = {
    header: null
  };

  gotoPage(page) {
    this.refs.viewPager.setPage(page);
  }

  onPageSelected({ position }) {
    this.setState({ activePage: position });
  }

  render() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Authentification" })]
    });

    return (
      <View style={{ flex: 1, backgroundColor: colors.PRIMARY }}>
        <IndicatorViewPager
          style={{ flex: 1 }}
          onPageSelected={this.onPageSelected.bind(this)}
          ref="viewPager"
        >
          {steps.map(s => (
            <View key={s.id}>
              <OnboardingScreen
                title={s.title}
                desc={s.desc}
                image={s.image}
                key={s.id}
              />
            </View>
          ))}
        </IndicatorViewPager>
        <View style={styles.button}>
          <Button
            onPress={() => {
              this.state.activePage === 2
                ? this.props.navigation.dispatch(resetAction)
                : this.gotoPage(this.state.activePage + 1);
            }}
            title={this.state.activePage === 2 ? "Commencer" : "Suivant"}
            titleColor={Platform.OS === "ios" ? colors.WHITE : colors.PRIMARY}
            color={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
            style={{
              width: 150,
              marginBottom: 10,
              alignSelf: "center"
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width: "100%",
    justifyContent: "center",
    padding: 5,
    height: "auto"
  }
});
