import React, { Component } from "react";
import {
  ScrollView,
  View,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import OrdersList from "../Orders/OrdersList";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
import { LocaleConfig } from "react-native-calendars";
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import * as mobx from "mobx";
import clientStore from "../../stores/clientStore";
import moment from "moment";
import _ from "lodash";

LocaleConfig.locales["fr"] = {
  monthNames: [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre"
  ],
  monthNamesShort: [
    "Janv.",
    "Févr.",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juil.",
    "Août",
    "Sept.",
    "Oct.",
    "Nov.",
    "Déc."
  ],
  dayNames: [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi"
  ],
  dayNamesShort: ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."]
};

LocaleConfig.defaultLocale = "fr";

let index = 0;
const data = [
  {
    no: index++,
    title: "Chemise délabrée",
    deliverDate: "16 Février 2018",
    deliverPlace: "Pikine"
  },
  {
    no: index++,
    title: "Pantalon Kaki",
    deliverDate: "20 Février 2018",
    deliverPlace: "Maristes"
  },
  {
    no: index++,
    title: "Boubou Gagnila",
    deliverDate: "25 Février 2018",
    deliverPlace: "Guédiawaye"
  },
  {
    no: index++,
    title: "Taille basse",
    deliverDate: "28 Février 2018",
    deliverPlace: "Dakar, Ville"
  }
];

@observer
export default class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "SunuCouture",
    tabBarLabel: "Dashboard",
    headerTitleStyle: { alignSelf: "center" },
    headerLeft: <View />,
    headerRight: (
      <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
        <View style={{ padding: 10 }}>
          <Icon
            name={Platform.OS === "ios" ? "ios-settings" : "md-settings"}
            size={30}
            color={colors.WHITE}
          />
        </View>
      </TouchableOpacity>
    ),
    tabBarIcon: ({ tintColor }) => (
      <Icon
        name={Platform.OS === "ios" ? "ios-apps" : "md-apps"}
        size={30}
        color={tintColor}
      />
    )
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedDate: "2018-03-26",
      orders: [],
      markedDays: []
    };
    console.log(this.state);
  }

  async _handleDayPress(day) {
    console.log("selected day", day);
    this.setState({ selectedDate: day });
    this.setState({ loading: true });
    let id = await AsyncStorage.getItem("TOKEN_ID");
    await clientStore.getTailorOrdersByDate(
      id,
      this.state.selectedDate.dateString
    );
    let orders = mobx.toJS(clientStore.tailorOrdersByDate);
    this.setState({ orders: orders });
    this.setState({ loading: false });
    console.log(orders);
  }

  async componentDidMount() {
    this.setState({ loading: true });
    let id = await AsyncStorage.getItem("TOKEN_ID");
    await clientStore.getTailorOrdersByDate(
      id,
      moment(new Date()).format("YYYY-MM-DD")
    );
    let orders = mobx.toJS(clientStore.tailorOrdersByDate);
    this.setState({ orders: orders });
    this.setState({ loading: false });
    console.log(orders);
    // let markedDays = {};
    // for (let i = 0; i < orders.length; i++) {
    //   let date = moment(new Date(orders[i].dateDeLivraison)).format(
    //     "YYYY-MM-DD"
    //   );
    //   let obj = {
    //     "": { marked: true, dotColor: "red", activeOpacity: 0 }
    //   };
    //   console.log(obj);
    //   _.assign(markedDays, obj);
    // }
    // console.log(markedDays);
    // this.setState({ markedDays: markedDays });
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          backgroundColor: colors.WHITE,
          padding: 5
        }}
      >
        <View style={{ flex: 1, minHeight: 100 }}>
          <Calendar
            // Initially visible month. Default = Date()
            /* current={"2018-02-15"} */
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={"2012-05-10"}
            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
            maxDate={"2020-05-30"}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={day => this._handleDayPress(day)}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={"dddd dd MMMM yyyy"}
            // Handler which gets executed when visible month changes in calendar. Default = undefined
            onMonthChange={month => {
              console.log("month changed", month);
            }}
            // Hide month navigation arrows. Default = false
            hideArrows={false}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={false}
            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
            // day from another month that is visible in calendar page. Default = false
            disableMonthChange={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Hide day names. Default = false
            hideDayNames={false}
            // Show week numbers to the left. Default = false
            showWeekNumbers={true}
            markedDates={{
              "2018-02-16": {
                marked: true,
                dotColor: "red",
                activeOpacity: 0
              },
              "2018-02-28": { marked: true, dotColor: "red", activeOpacity: 0 },
              "2018-02-20": { marked: true, dotColor: "red", activeOpacity: 0 },
              "2018-02-25": { marked: true, dotColor: "red", activeOpacity: 0 }
            }}
          />
        </View>

        <View style={{ flex: 1 }}>
          {this.state.loading === true ? (
            <ActivityIndicator size="large" color={colors.SECONDARY} />
          ) : (
            <View
              style={{
                flex: 1,
                padding: 5,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Button
                title="Voir la liste complète"
                color={colors.WHITE}
                titleColor={
                  Platform.OS === "ios" ? colors.SECONDARY : colors.SECONDARY
                }
                style={{ width: 200, borderRadius: 2 }}
                onPress={() => {}}
              />
              <OrdersList
                orders={this.state.orders}
                onPress={order =>
                  this.props.navigation.navigate("OrderDetails", {
                    order: order
                  })
                }
              />
            </View>
          )}
        </View>
        {/* <ActionButton
          buttonColor={colors.SECONDARY_LIGHT}
          onPress={() => this.props.navigation.navigate("OrderForm")}
          fixNativeFeedbackRadius={true}
        /> */}
      </ScrollView>
    );
  }
}
