import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import PropTypes from "prop-types";
import Card from "../../components/common/Card";
import colors from "~theme/colors";

export default class Order extends Component {
  render() {
    const { order } = this.props;
    const date = new Date(order.dateDeLivraison);
    return (
      <TouchableOpacity activeOpacity={0.6} onPress={this.props.onPress}>
        <Card elevation={2} style={styles.card}>
          <Text style={{ color: colors.WHITE }}>Commande No: {order.id}</Text>
          <Text style={{ color: colors.WHITE }}>
            {date.toLocaleDateString()}
          </Text>
          <Text style={{ color: colors.WHITE }}>Prix: {order.prix} Frcs</Text>
          <Text style={{ color: colors.WHITE }}>
            Avance: {order.avancePrix} Frcs
          </Text>
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: colors.PRIMARY,
    borderRadius: 3,
    width: 120,
    height: 120,
    margin: 5,
    justifyContent: "center",
    alignItems: "center"
  }
});
