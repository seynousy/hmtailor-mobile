import React, { Component } from "react";
import { FlatList, View } from "react-native";
import Order from "./Order";
import colors from "~theme/colors";

export default class OrdersList extends Component {
  _renderItem = ({ item }) => (
    <Order onPress={() => this.props.onPress(item)} order={item} />
  );

  _keyExtractor = (item, index) => item.id;

  render() {
    return (
      <FlatList
        data={this.props.orders}
        extraData={this.state}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        horizontal={true}
      />
    );
  }
}
