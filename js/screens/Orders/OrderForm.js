import React, { Component } from "react";
import { View, TextInput, Platform, Text, AsyncStorage } from "react-native";
import { IndicatorViewPager } from "rn-viewpager";
import ModalSelector from "react-native-modal-selector";
import { NavigationActions } from "react-navigation";
import Input from "../../components/common/Input";
import Button from "../../components/common/Button";
import colors from "~theme/colors";

import * as mobx from "mobx";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class OrderForm extends Component {
  static navigationOptions = {
    title: "Ajouter commande"
  };
  constructor(props) {
    super(props);
    this.state = {
      selectedClient: "",
      idClient: null,
      dateDeLivraison: "",
      avancePrix: "",
      clients: []
    };
  }

  async componentDidMount() {
    let token = await AsyncStorage.getItem("TOKEN_ID");
    await clientStore.getTailorClients(token);
    this.setState({ clients: clientStore.clients });
  }

  async _handleSubmit() {
    const gotoDashboard = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Dashboard" })]
    });
    let data = {
      avancePrix: Number(this.state.avancePrix),
      idModel: this.props.navigation.state.params.model.id,
      idClient: this.state.idClient,
      dateDeLivraison: this.state.dateDeLivraison,
      prix: this.props.navigation.state.params.model.prix
    };
    await clientStore.addOrder(data);
    this.props.navigation.navigate("Dashboard");
  }

  render() {
    let index = 0;
    const dataClients = [{ key: index++, section: true, label: "Clients" }];

    for (let i = 0; i < this.state.clients.length; i++) {
      let entry = {
        key: index++,
        label: this.state.clients[i].prenom + " " + this.state.clients[i].nom,
        id: this.state.clients[i].id
      };
      dataClients.push(entry);
    }
    return (
      <View
        style={{ flex: 1, justifyContent: "space-between", overflow: "hidden" }}
      >
        <View style={{ flex: 1, backgroundColor: colors.WHITE, padding: 5 }}>
          <View style={{ flex: 1, justifyContent: "center" }}>
            <ModalSelector
              data={dataClients}
              initValue=""
              onChange={option => {
                this.setState({
                  selectedClient: option.label,
                  idClient: option.id
                });
              }}
            >
              <Input
                placeholder="Client"
                style={{ backgroundColor: colors.PRIMARY_DARK }}
                iconStyle={{ backgroundColor: colors.PRIMARY }}
                icon={Platform.OS === "ios" ? "ios-person" : "md-person"}
                value={this.state.selectedClient}
              />
            </ModalSelector>
            <Input
              placeholder="Avance"
              keyboardType="numeric"
              style={{ backgroundColor: colors.PRIMARY_DARK }}
              iconStyle={{ backgroundColor: colors.PRIMARY }}
              icon={Platform.OS === "ios" ? "ios-pricetags" : "md-pricetags"}
              value={this.state.avancePrix}
              onChangeText={text => this.setState({ avancePrix: text })}
            />
            <Input
              placeholder="Date de livraison: aaaa-mm-jj"
              style={{ backgroundColor: colors.PRIMARY_DARK }}
              iconStyle={{ backgroundColor: colors.PRIMARY }}
              icon={Platform.OS === "ios" ? "ios-pricetags" : "md-pricetags"}
              value={this.state.dateDeLivraison}
              onChangeText={text => this.setState({ dateDeLivraison: text })}
            />
          </View>
          <Button
            title="Enregistrer"
            color={colors.PRIMARY}
            titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
            style={{
              width: "100%",
              height: 40,
              borderRadius: 2,
              marginTop: 15,
              elevation: 2
            }}
            onPress={() => this._handleSubmit()}
          />
        </View>
      </View>
    );
  }
}
