import React, { Component } from "react";
import { View } from "react-native";
import ComingSoon from "../ComingSoon";

export default class OrderDetails extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Commande N°" + navigation.state.params.order.id
  });
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ComingSoon title="Orders Details" />
      </View>
    );
  }
}
