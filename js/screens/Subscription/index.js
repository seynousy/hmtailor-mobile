import React, { Component } from "react";
import { View, Text, Platform, Image, ScrollView } from "react-native";
import { NavigationActions } from "react-navigation";
import RadioForm from "react-native-simple-radio-button";
import Input from "../../components/common/Input";
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

let radio_props = [
  { label: "Homme    ", value: "M" },
  { label: "Femme", value: "F" }
];

@observer
export default class Subscription extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      nom: "",
      prenom: "",
      adresse: "",
      numTel: "",
      sexe: "M",
      email: "",
      motDePasse: "",
      confirmation: ""
    };
  }
  async _handleSubscription() {
    const goToLogin = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Authentification" })]
    });

    let tailor = {
      nom: this.state.nom,
      prenom: this.state.prenom,
      adresse: this.state.adresse,
      numTel: this.state.numTel,
      sexe: this.state.sexe,
      email: this.state.email,
      motDePasse: this.state.motDePasse,
      confirmation: this.state.confirmation
    };

    await clientStore.addTailor(tailor);
    if (clientStore.tailorAddedReq.status === 200) {
      alert("Ajouté");
      this.props.navigation.dispatch(goToLogin);
    } else {
      alert("Erreur");
    }
  }
  render() {
    return (
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: colors.PRIMARY
        }}
        contentContainerStyle={{
          flex: 1,
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        <View style={{ width: 70, height: 70 }}>
          <Image
            source={require("~assets/images/logo.png")}
            resizeMode="contain"
            style={{ width: 70, height: 70 }}
          />
        </View>
        <View style={{ width: "90%" }}>
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Nom"
            icon={Platform.OS === "ios" ? "ios-person-outline" : "md-person"}
            value={this.state.username}
            onChangeText={text => this.setState({ nom: text })}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Prénom"
            icon={Platform.OS === "ios" ? "ios-person-outline" : "md-person"}
            value={this.state.username}
            onChangeText={text => this.setState({ prenom: text })}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Adresse"
            icon={Platform.OS === "ios" ? "ios-pin-outline" : "md-pin"}
            value={this.state.username}
            onChangeText={text => this.setState({ adresse: text })}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="phone-pad"
            placeholder="Numéro de Téléphone"
            icon={Platform.OS === "ios" ? "ios-call-outline" : "md-call"}
            value={this.state.username}
            onChangeText={text => this.setState({ numTel: text })}
          />
          <RadioForm
            style={{ padding: 10, alignSelf: "center" }}
            formHorizontal={true}
            radio_props={radio_props}
            initial={0}
            buttonColor={colors.PRIMARY_DARK}
            labelColor={colors.WHITE}
            selectedButtonColor={colors.PRIMARY_DARK}
            selectedLabelColor={colors.WHITE}
            onPress={value => {
              this.setState({ sexe: value });
            }}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Email"
            keyboardType="email-address"
            icon={Platform.OS === "ios" ? "ios-at-outline" : "md-at"}
            value={this.state.email}
            onChangeText={text => this.setState({ email: text })}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Mot de passe"
            secureTextEntry={true}
            icon={Platform.OS === "ios" ? "ios-lock-outline" : "md-lock"}
            value={this.state.password}
            onChangeText={text => this.setState({ motDePasse: text })}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Confirmer Mot de passe"
            secureTextEntry={true}
            icon={Platform.OS === "ios" ? "ios-lock-outline" : "md-lock"}
            value={this.state.password}
            onChangeText={text => this.setState({ confirmation: text })}
          />
        </View>
        <Button
          title="S'inscrire"
          titleColor={Platform.OS === "ios" ? colors.WHITE : colors.PRIMARY}
          color={colors.WHITE}
          onPress={() => this._handleSubscription()}
          style={{ borderRadius: 2, elevation: 2, width: "90%" }}
        />
      </ScrollView>
    );
  }
}
