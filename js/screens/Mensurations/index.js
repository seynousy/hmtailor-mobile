import React, { Component } from "react";
import { ScrollView, Text, ActivityIndicator } from "react-native";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";
import * as mobx from "mobx";
import colors from "~theme/colors";

@observer
export default class Mensurations extends Component {
  static navigationOptions = {
    title: "Mensurations"
  };

  constructor(props) {
    super(props);
    this.state = {
      mensurations: [],
      loading: false
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    await clientStore.getMensurations();
    let mensurations = mobx.toJS(clientStore.allMensurations);
    this.setState({ mensurations: mensurations });
    this.setState({ loading: false });
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{ flex: 1, backgroundColor: colors.WHITE }}
      >
        {this.state.loading === true ? (
          <ActivityIndicator size="large" color={colors.SECONDARY} />
        ) : (
          this.state.mensurations.map((m, i) => (
            <Text
              style={{ fontSize: 20, padding: 10, color: colors.BLACK }}
              key={i}
            >
              {m.nom}
            </Text>
          ))
        )}
      </ScrollView>
    );
  }
}
