import React, { Component } from "react";
import { ScrollView, Platform } from "react-native";
import { NavigationActions } from "react-navigation";
import Input from "../../components/common/Input";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class MensurationForm extends Component {
  static navigationOptions = {
    title: "Mensuration"
  };
  constructor(props) {
    super(props);
    this.state = {
      nom: ""
    };
  }

  async _handleSubmit() {
    // let id = await AsyncStorage.getItem("TOKEN_ID");
    // console.log("ID" + id);
    // let data = {
    //   nom: this.state.nom,
    //   idTailleur: id
    // };
    await clientStore.addMensuration({ nom: this.state.nom });
    this.props.navigation.navigate("Mensurations");
  }
  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          padding: 10,
          backgroundColor: colors.WHITE,
          flex: 1,
          justifyContent: "center"
        }}
      >
        <Input
          placeholder="Libelle de la mensuration"
          style={{ backgroundColor: colors.PRIMARY_DARK }}
          iconStyle={{ backgroundColor: colors.PRIMARY }}
          icon={Platform.OS === "ios" ? "ios-create" : "md-create"}
          onChangeText={text => this.setState({ nom: text })}
          value={this.state.nom}
        />
        <Button
          title="Ajouter"
          color={colors.PRIMARY}
          titleColor={Platform.OS === "ios" ? colors.PRIMARY : colors.WHITE}
          style={{
            width: 150,
            borderRadius: 2,
            marginTop: 15,
            elevation: 2,
            alignSelf: "center"
          }}
          onPress={() => this._handleSubmit()}
        />
      </ScrollView>
    );
  }
}
