import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  Platform,
  Image,
  TouchableHighlight,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import { NavigationActions } from "react-navigation";
import Input from "../../components/common/Input";
import Icon from "react-native-vector-icons/Ionicons";
import Button from "../../components/common/Button";
import colors from "~theme/colors";
import { observer } from "mobx-react";
import clientStore from "../../stores/clientStore";

@observer
export default class Authentification extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loggingIn: false
    };
  }

  async _login() {
    this.setState({ loggingIn: true });
    const goToDashboard = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "LoginGuard" })]
    });
    let username = this.state.username;
    let password = this.state.password;
    await clientStore.tailorLogin(username, password);
    if (clientStore.currentTailor) {
      AsyncStorage.setItem("TOKEN_ID", clientStore.currentTailor.toString());
      this.props.navigation.navigate("LoginGuard");
    } else {
      alert("Mauvaise combinaison");
    }
    this.setState({ loggingIn: false });
  }

  render() {
    return (
      <ScrollView
        contentContainerStyle={{
          flex: 1,
          backgroundColor: colors.PRIMARY,
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        <View style={{ width: 200, height: 200 }}>
          <Image
            source={require("~assets/images/logo.png")}
            resizeMode="contain"
            style={{ width: 200, height: 200 }}
          />
        </View>
        <View style={{ width: "90%" }}>
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="email-address"
            placeholder="Email ou Nom d'utilisateur"
            value={this.state.username}
            onChangeText={text => this.setState({ username: text })}
            icon={Platform.OS === "ios" ? "ios-person" : "md-person"}
          />
          <Input
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry={true}
            placeholder="Mot de passe"
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })}
            icon={Platform.OS === "ios" ? "ios-lock" : "md-lock"}
          />
        </View>
        <Button
          disabled={this.state.loggingIn}
          title="Se Connecter"
          titleColor={
            Platform.OS === "ios" ? colors.WHITE : colors.PRIMARY_DARK
          }
          color={colors.WHITE}
          onPress={() => this._login()}
          style={{ borderRadius: 2, elevation: 2, width: "90%" }}
        />
        {this.state.loggingIn === true ? (
          <ActivityIndicator size="large" color={colors.WHITE} />
        ) : null}
        <TouchableHighlight
          underlayColor={colors.TRANSPARENT}
          onPress={() => this.props.navigation.navigate("Subscription")}
        >
          <Text
            style={{
              fontSize: 17,
              fontWeight: "bold",
              color: colors.WHITE,
              textDecorationLine: "underline"
            }}
          >
            Pas encore de compte? S'inscrire !
          </Text>
        </TouchableHighlight>
      </ScrollView>
    );
  }
}
