import React, { Component } from "react";
import { View, Platform } from "react-native";
import PropTypes from "prop-types";

export default class Card extends Component {
  render() {
    return (
      <View
        style={
          Platform.OS === "ios"
            ? [
                {
                  shadowOffset: {
                    width: this.props.elevation / 2,
                    height: this.props.elevation / 2
                  },
                  shadowColor: "grey",
                  shadowOpacity: this.props.elevation === 0 ? 0 : 1
                },
                this.props.style
              ]
            : [
                {
                  elevation: this.props.elevation,
                  backgroundColor: "white"
                },
                this.props.style
              ]
        }
      >
        {this.props.children}
      </View>
    );
  }
}

Card.defaultProps = {
  elevation: 4
};

Card.propTypes = {
  elevation: PropTypes.number.isRequired,
  children: PropTypes.node
};
