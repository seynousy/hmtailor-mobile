import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TouchableNativeFeedback,
  Text,
  View,
  Button as ButtonIos,
  Platform
} from "react-native";
import { TextButton } from "react-native-material-buttons";

export default class Button extends Component {
  render() {
    return Platform.OS === "android" ? (
      <TextButton
        titleColor={this.props.titleColor}
        title={this.props.title}
        color={this.props.color}
        {...this.props}
      />
    ) : (
      <ButtonIos
        {...this.props}
        title={this.props.title}
        color={this.props.titleColor}
      />
    );
  }
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired
};
