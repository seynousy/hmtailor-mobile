import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

export default class CatalogFolder extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <TouchableOpacity onPress={this.props.onPress}>
          <Image source={require("~assets/images/folder.png")} />
          <Text style={{ textAlign: "center" }}>{this.props.label}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

CatalogFolder.propTypes = {
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func
};
