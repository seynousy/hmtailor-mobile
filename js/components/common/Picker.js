import React, { Component } from "react";
import { Picker as RNPicker, StyleSheet, View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

export default class Picker extends Component {
  static defaultProps = {
    items: []
  };
  render() {
    const { items } = this.props;
    return (
      <View style={styles.container}>
        <View style={[styles.icon, this.props.iconStyle]}>
          <Icon name={this.props.icon} color="white" size={25} />
        </View>
        <RNPicker
          itemStyle={styles.item}
          {...this.props}
          style={[styles.picker, this.props.style]}
        >
          {items.map(item => (
            <RNPicker.Item
              label={item.label}
              value={item.value}
              key={item.value}
            />
          ))}
        </RNPicker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "stretch",
    marginBottom: 1
  },
  picker: {
    flex: 1,
    color: "white",
    backgroundColor: "rgba(0,0,0,0.4)",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    height: 57
  },
  item: {
    fontWeight: "bold"
  },
  icon: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: "rgba(0,0,0,0.3)",
    justifyContent: "center",
    padding: 15,
    width: 50
  }
});

Picker.propTypes = {
  items: PropTypes.array,
  icon: PropTypes.string,
  iconStyle: PropTypes.object,
  style: PropTypes.object
};
