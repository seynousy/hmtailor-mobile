import { observable } from "mobx";
import * as mobx from "mobx";
import { Platform } from "react-native";
import moment from "moment";

const LOCAL_URL =
  Platform.OS === "ios"
    ? "http://localhost:8080/"
    : "http://192.168.43.107:8080/";

class ClientStore {
  @observable clientAdded = {};
  @observable clients = [];
  @observable currentTailor;
  @observable currentTailorDetails = {};
  @observable tailorAddedReq = {};
  @observable modelAddedReq = {};
  @observable tailorCatalogs = [];
  @observable catalogModels = [];
  @observable dressTypes = [];
  @observable tailorOrders = [];
  @observable tailorOrdersByDate = [];
  @observable clientMensurations = [];
  @observable allMensurations = [];

  async addTailor(tailor) {
    try {
      let response = await fetch(LOCAL_URL + "tailleurs", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(tailor)
      });
      this.tailorAddedReq = response;
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de l'inscription");
    }
  }

  async getTailorbyId(id) {
    try {
      let response = await fetch(LOCAL_URL + "tailleurs/" + id);
      this.currentTailorDetails = await response.json();
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue après la connexion");
    }
  }

  async tailorLogin(email, password) {
    let tailor = {
      email: email,
      motDePasse: password
    };
    try {
      let response = await fetch(LOCAL_URL + "login", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(tailor)
      });
      let body = await response.json();
      this.currentTailor = body.idUser;
    } catch (e) {
      console.error(e);
      alert("Combinaison erronée !");
    }
  }

  async getTailorClients(tailorId) {
    try {
      let response = await fetch(LOCAL_URL + "tailleurs/" + tailorId);
      let body = await response.json();
      this.clients = body.clients;
    } catch (e) {
      console.error(e);
      alert(
        "Une erreur est survenue lors de la récupération des clients de ce tailleur"
      );
    }
  }

  async addTailorClient(client) {
    fetch(LOCAL_URL + "clients", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(client)
    })
      .then(response => {
        this.clientAdded = response.body;
        if (response.status === 200) {
          alert("Client ajouté avec succès");
        } else {
          alert("Erreur lors de l'ajout du client");
        }
      })
      .catch(e => {
        console.error(e);
      });
  }

  async removeTailorClient(clientId) {
    try {
      let response = await fetch(LOCAL_URL + "clients/" + clientId, {
        method: "DELETE"
      });
      if (response.status === 200) {
        alert("Client supprimé avec succès");
      } else {
        alert("Erreur lors de la suppression du client");
      }
    } catch (e) {
      console.error(e);
    }
  }

  async getTailorCatalogs(tailorId) {
    try {
      let response = await fetch(LOCAL_URL + "tailleurs/" + tailorId);
      let result = await response.json();
      this.tailorCatalogs = result.catalogues;
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de la récupération des catalogues");
    }
  }

  async getCatalogModels(catalogId) {
    try {
      let response = await fetch(LOCAL_URL + "catalogues/" + catalogId);
      let result = await response.json();
      this.catalogModels = result.models;
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de la récupération des catalogues");
    }
  }

  async updateModelImage(image, id) {
    let data = new FormData();
    data.append("id", id);
    data.append("image", {
      uri: image.path,
      type: image.mime,
      name: "model"
    });
    // let data = {
    //   image: image,
    //   id: id
    // };
    try {
      let response = await fetch(LOCAL_URL + "models/image", {
        method: "PUT",
        headers: {
          "Content-Type": "multipart/form-data"
        },
        body: data
      });
      console.log(response);
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de l'ajout de l'image du modèle");
    }
  }

  async addModel(data) {
    try {
      let response = await fetch(LOCAL_URL + "models", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      let id = await response.json();
      if (response.status === 200) {
        await this.updateModelImage(data.image, id);
      }
      console.log(response);
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de l'ajout du modèle");
    }
  }

  async addCatalog(data) {
    try {
      let response = await fetch(LOCAL_URL + "catalogues", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      console.log(response);
      if (response.status === 200) {
        alert("Ajouté");
      }
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de l'ajout de catalogue");
    }
  }

  async getDressTypes() {
    try {
      let response = await fetch(LOCAL_URL + "typeDhabits");
      let result = await response.json();
      this.dressTypes = result;
    } catch (e) {
      alert(
        "une erreur est survenue lors de la récupération des types d'habits"
      );
      console.error(e);
    }
  }

  async getTailorOrders(tailorId) {
    try {
      let response = await fetch(LOCAL_URL + "commandes/tailleur/" + tailorId);
      let body = await response.json();
      this.tailorOrders = body;
      console.log(response);
    } catch (e) {
      console.error(e);
      alert(
        "Une erreur est survenue lors de la récupération des commandes du jour"
      );
    }
  }

  async getTailorOrdersByDate(tailorId, date) {
    try {
      let response = await fetch(LOCAL_URL + "commandes/tailleur/" + tailorId);
      let body = await response.json();
      this.tailorOrdersByDate = body.filter(
        o => moment(new Date(o.dateDeLivraison)).format("YYYY-MM-DD") === date
      );
      console.log(response);
    } catch (e) {
      console.error(e);
      alert(
        "Une erreur est survenue lors de la récupération des commandes du jour"
      );
    }
  }

  async addOrder(order) {
    try {
      let response = await fetch(LOCAL_URL + "commandes", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(order)
      });
      if (response.status === 200) {
        alert("Commande bien enregistrée");
      } else {
        alert("Erreur lors de l'ajout de la commande");
      }
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de l'inscription");
    }
  }

  async getClientMensurations(idClient) {
    try {
      let response = await fetch(LOCAL_URL + "clients/" + idClient);
      let body = await response.json();
      this.clientMensurations = body.mensurationValues;
    } catch (e) {
      alert(
        "Une erreur est survenue lors de la récupération des mensurations de ce client"
      );
      console.error(e);
    }
  }

  async addMensuration(mensuration) {
    try {
      let response = await fetch(LOCAL_URL + "mensurations", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(mensuration)
      });
      console.log(response);
      if (response.status === 200) {
        alert("Mensuraion ajoutée");
      }
    } catch (e) {
      alert("Une erreur est survenue lors de l'ajout de la mensuration");
    }
  }

  async getMensurations() {
    try {
      let response = await fetch(LOCAL_URL + "mensurations");
      this.allMensurations = await response.json();
      console.log(response);
    } catch (e) {
      console.error(e);
      alert("Une erreur est survenue lors de la récupération des mensurations");
    }
  }
}

const clientStore = new ClientStore();
export default clientStore;
