import React, { Component } from "react";
import { View, StatusBar, StyleSheet } from "react-native";
import { StackNavigator, TabNavigator } from "react-navigation";
import Onboarding from "./js/screens/Onboarding";
import Subscription from "./js/screens/Subscription";
import LoginGuard from "./js/screens/LoginGuard";
import Authentification from "./js/screens/Authentification";
import Dashboard from "./js/screens/Dashboard";
import Catalog from "./js/screens/Catalog";
import Clients from "./js/screens/Clients";
import ClientDetails from "./js/screens/Clients/ClientDetails";
import ModelDetails from "./js/screens/Catalog/ModelDetails";
import OrderDetails from "./js/screens/Orders/OrderDetails";
import ClientForm from "./js/screens/Clients/ClientForm";
import OrderForm from "./js/screens/Orders/OrderForm";
import ModelForm from "./js/screens/Catalog/ModelForm";
import CatalogForm from "./js/screens/Catalog/CatalogForm";
import CatalogDetails from "./js/screens/Catalog/CatalogDetails";
import Mensurations from "./js/screens/Mensurations";
import MensurationForm from "./js/screens/Mensurations/MensurationForm";
import Settings from "./js/screens/Settings";
import colors from "./js/theme/colors";
import clientStore from "./js/stores/clientStore";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.PRIMARY_DARK} />
        <AppNavigation />
      </View>
    );
  }
}

const HomeNavigation = TabNavigator(
  {
    Catalog: { screen: Catalog },
    Dashboard: { screen: Dashboard },
    Clients: { screen: Clients }
  },
  {
    swipeEnabled: true,
    tabBarPosition: "bottom",
    tabBarOptions: {
      activeTintColor: colors.SECONDARY_DARK,
      inactiveTintColor: colors.WHITE,
      style: {
        backgroundColor: colors.PRIMARY
      },
      labelStyle: {
        fontWeight: "bold"
      },
      indicatorStyle: {
        backgroundColor: colors.PRIMARY
      },
      showIcon: true,
      showLabel: false,
      upperCaseLabel: false
    },
    initialRouteName: "Dashboard",
    order: ["Catalog", "Dashboard", "Clients"]
  }
);

const AppNavigation = StackNavigator(
  {
    LoginGuard: { screen: LoginGuard },
    Onboarding: { screen: Onboarding },
    Authentification: { screen: Authentification },
    Subscription: { screen: Subscription },
    Dashboard: { screen: HomeNavigation },
    ClientDetails: { screen: ClientDetails },
    ModelDetails: { screen: ModelDetails },
    OrderDetails: { screen: OrderDetails },
    ClientForm: { screen: ClientForm },
    ModelForm: { screen: ModelForm },
    CatalogDetails: { screen: CatalogDetails },
    OrderForm: { screen: OrderForm },
    CatalogForm: { screen: CatalogForm },
    Mensurations: { screen: Mensurations },
    MensurationForm: { screen: MensurationForm },
    Settings: { screen: Settings }
  },
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: colors.PRIMARY
      },
      headerTintColor: colors.WHITE
    })
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
